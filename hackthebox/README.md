# About
PDF Files are named after each machine on hackthebox. 

Some files are gpg encrypted due to the boxes being active. When a machine becomes retired, I will eventually get around to decrypting the PDFs. If I forget and you want access to the PDF, just open an issue. 

# Decrypting PDFs
The passwords for each GPG encrypted file is the contents of root's shadow entry on that box, between the first and second colons, or more formally, between the username (root), and the last password change date 

Example /etc/shadow entry
```
root:$6$AMvgOmRCNzBloX3T$rd5nRPwkBPHenf6aRfa2Fask2aAsfFAS4Axf2fA29182sxDrfaX.McDSfawkplkXfsMvsFDS231Asf84x:18439:0:99999:7:::
```

The password would be `$6$AMvgOmRCNzBloX3T$rd5nRPwkBPHenf6aRfa2Fask2aAsfFAS4Axf2fA29182sxDrfaX.McDSfawkplkXfsMvsFDS231Asf84x`

GPG command to decrypt
```
gpg -d filename.pdf.gpg > filename.pdf
````

